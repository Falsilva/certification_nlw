## Microserviço de Certificações
Projeto construído durante o evento NLW Expert da [Rockerseat](https://www.rocketseat.com.br/) (Trilha Java) em ***Fevereiro/2024***.

É uma aplicação back-end em Java, tratando-se de um microserviço de certificações, o qual um usuário cadastrado com um email realiza um teste para a tecnologia escolhida, mostrando o resultado com uma nota e também um ranking Top 10.

> JAVA | Spring Boot | Maven | JPA | PostgreSQL | Docker | VSCode






